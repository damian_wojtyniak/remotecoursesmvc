﻿using RemoteCourses.Interfaces;
using RemoteCourses.Repositories.EntityFramework;

namespace RemoteCourses.Repositories
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly CourseContext _context;
        public ICoursesRepository Courses { get; set; }
        public IAnswersRepository Answers { get; set; }
        public IContentsRepository Contents { get; set; }
        public IQuestionsRepository Questions { get; set; }
        public IResultsRepository Results { get; set; }
        public ISectionsRepository Sections { get; set; }
        public IStudentsRepository Students { get; set; }
        
        public UnitOfWork(CourseContext context)
        {
            _context = context;
            Courses = new EFCoursesRepository(_context);
            Students = new EFStudentsRepository(_context);
            Sections = new EFSectionsRepository(_context);
            Results = new EFResultsRepository(_context);
            Questions = new EFQuestionsRepository(_context);
            Answers = new EFAnswersRepository(_context);
            Contents = new EFContentsRepository(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
