﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFQuestionsRepository : Repository<Question>, IQuestionsRepository
    {
        public EFQuestionsRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
