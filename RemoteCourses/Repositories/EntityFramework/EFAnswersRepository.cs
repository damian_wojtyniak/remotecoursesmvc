﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFAnswersRepository : Repository<Answer>, IAnswersRepository
    {
        public EFAnswersRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
