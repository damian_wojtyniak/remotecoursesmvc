﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFStudentsRepository : Repository<Student>, IStudentsRepository
    {
        public EFStudentsRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
