﻿using RemoteCourses.Interfaces;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFSectionsRepository : Repository<Section>, ISectionsRepository
    {
        public EFSectionsRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
