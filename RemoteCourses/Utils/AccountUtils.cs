﻿using RemoteCourses.Models;
using RemoteCourses.Repositories.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace RemoteCourses.Utils
{
    public class AccountUtils
    { 
        public static bool isUserRegistered(CourseContext context, Student newStudent)
        {
            return context.Students.Any(u => u.Name == newStudent.Name || u.Email == newStudent.Email);
        }

        public static bool UserAuthentication(CourseContext context, Student user)
        {
            var validUser = context.Students.Any(u => u.Name == user.Name && user.Password == u.Password);

            if (validUser)
            {
                FormsAuthentication.SetAuthCookie(user.Name, true);
            }

            return validUser;
        }

        public static Student AuthenticatedUser(HttpCookie cookie)
        {
            FormsAuthenticationTicket ticket;
            Student user = null;

            if (cookie != null && cookie.Value != null && cookie.Value.Length > 0)
            {
                ticket = FormsAuthentication.Decrypt(cookie.Value);
                user = new Student
                {
                    Name = ticket.Name
                };
                FormsAuthentication.SetAuthCookie(user.Name, true);
            }
             
            return user;
        }
    }
}