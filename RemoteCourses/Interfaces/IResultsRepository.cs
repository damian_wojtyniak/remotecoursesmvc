﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface IResultsRepository : IRepository<Result>
    {

    }
}
