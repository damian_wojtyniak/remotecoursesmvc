﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface ISectionsRepository : IRepository<Section>
    {

    }
}
