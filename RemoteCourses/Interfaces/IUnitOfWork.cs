﻿using System;

namespace RemoteCourses.Interfaces
{
    interface IUnitOfWork : IDisposable
    {
        ICoursesRepository Courses { get; }
        IAnswersRepository Answers { get; }
        IContentsRepository Contents { get; }
        IQuestionsRepository Questions { get; }
        IResultsRepository Results { get; }
        ISectionsRepository Sections { get; }
        IStudentsRepository Students { get; }

        void Save();
    }
}
