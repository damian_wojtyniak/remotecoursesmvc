﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface IQuestionsRepository : IRepository<Question>
    {

    }
}
