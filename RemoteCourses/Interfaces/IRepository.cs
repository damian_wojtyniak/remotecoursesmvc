﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace RemoteCourses.Interfaces
{
    interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(int id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entries);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entries);
        void Update(TEntity entity);
    }
}
