﻿using RemoteCourses.Models;

namespace RemoteCourses.Interfaces
{
    interface IAnswersRepository : IRepository<Answer>
    {

    }
}
