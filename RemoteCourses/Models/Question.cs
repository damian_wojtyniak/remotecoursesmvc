﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    public class Question
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Text { get; set; }
        public string CorrectAnswer { get; set; }

        public ICollection<Answer> Answers { get; set; }
        public Section Sections { get; set; }
    }
}
