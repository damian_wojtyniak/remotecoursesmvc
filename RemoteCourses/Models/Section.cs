﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    public class Section
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string SectionName { get; set; }

        public Course Courses { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}
