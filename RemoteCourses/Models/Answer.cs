﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    public class Answer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string AnswerText { get; set; }

        public Question Questions { get; set; }
    }
}
