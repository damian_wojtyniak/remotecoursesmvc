﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    public class Content
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContentText { get; set; }

        public Section Sections { get; set; }
    }
}
