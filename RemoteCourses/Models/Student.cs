﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RemoteCourses.Models
{
    public class Student
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Result> Results { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
    }
}
