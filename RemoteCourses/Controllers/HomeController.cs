﻿using RemoteCourses.Repositories.EntityFramework;
using System.Web.Mvc;

namespace RemoteCourses.Controllers
{
    public class HomeController : Controller
    {
        private CourseContext db = new CourseContext();

        public ActionResult Index()
        {
            var courses = db.Courses;

            return View(courses);
        }
    }
}