﻿using System.Web.Mvc;
using System.Web.Security;
using RemoteCourses.Models;
using RemoteCourses.Repositories.EntityFramework;
using RemoteCourses.Utils;

namespace RemoteCourses.Controllers
{
    public class StudentsController : Controller
    {
        private CourseContext db = new CourseContext();

        public StudentsController()
        {

        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            var newUser = new Student();

            return View(newUser);
        }

        [HttpPost]
        public ActionResult Login(Student user)
        {
            AccountUtils.UserAuthentication(db, user);

            return View();
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            var newUser = new Student();

            return View(newUser);
        }

        [HttpPost]
        public ActionResult Register(Student user)
        {
            if (AccountUtils.isUserRegistered(db, user))
            {
                TempData["Message"] = "Nazwa użytkownika lub email są już wykorzystywane przez innego uzytkownika.";
                return View();
            }
            else
            {
                db.Students.Add(new Student
                {
                    Name = user.Name,
                    Email = user.Email,
                    Password = user.Password
                });

                db.SaveChanges();

                if (AccountUtils.UserAuthentication(db, user))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Message"] = "Nieporawne dane logowania.";
                    return View();
                }
            }
        }

        public ActionResult UserAuth()
        {
            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            
            Student user = AccountUtils.AuthenticatedUser(cookie);
            
            return PartialView(user);
        }
    }
}
