﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using RemoteCourses.Models;
using RemoteCourses.Repositories.EntityFramework;
using RemoteCourses.Utils;

namespace RemoteCourses.Controllers
{
    public class QuestionsController : Controller
    {
        private CourseContext db = new CourseContext();
        private IEnumerable<Question> _questions;
        private Section _section;

        // GET: Questions
        public ActionResult Index(string sectionId)
        {
            if (sectionId != null)
            {
                var id = int.Parse(sectionId);
                var questions = db.Questions.Where(q => q.Sections.Id == id);
                _questions = questions;
            }

            return View(_questions.ToList());
        }

        public ActionResult QuizForm(string question1, string question2, string question3)
        {
            var studentName = GetAuthStudent();
            var student = db.Students.FirstOrDefault(s => s.Name == studentName);
            //var sections = db.Sections;

            string[] outputQuestion1 = question1.Split(';');
            string[] outputQuestion2 = question2.Split(';');
            string[] outputQuestion3 = question3.Split(';');

            Dictionary<int, string> questiondict = new Dictionary<int, string>() 
            { 
                { int.Parse(outputQuestion1[0]), outputQuestion1[1] },
                { int.Parse(outputQuestion2[0]), outputQuestion2[1] },
                { int.Parse(outputQuestion3[0]), outputQuestion3[1] }
            };

            int correctAnswerCounter = 0;
            int allAnswers = 0;
            int sectionId = 0;

            foreach (var compare in questiondict)
            {
                var question = db.Questions.FirstOrDefault(q => q.Id == compare.Key);

                if (question.CorrectAnswer == compare.Value)
                {
                    correctAnswerCounter++;
                    allAnswers++;
                }
                else
                {
                    allAnswers++;
                }

                sectionId = db.Sections.FirstOrDefault(s => s.Questions.FirstOrDefault(q => q.Id == question.Id).Sections.Id == s.Id).Id;
                _section = db.Sections.FirstOrDefault(s => s.Questions.FirstOrDefault(q => q.Id == question.Id).Sections.Id == s.Id);
            }

            var result = new Result() { Score = correctAnswerCounter, TotalQuestions = allAnswers, DateTaken = DateTime.Now, Student_Id = student.Id, Sections = _section };
            db.Results.Add(result);
            db.SaveChanges();

            return RedirectToAction("Index", "Results");
        }

        // GET: Questions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // GET: Questions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Questions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Text,CorrectAnswer")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Questions.Add(question);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(question);
        }

        // GET: Questions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Text,CorrectAnswer")] Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(question);
        }

        // GET: Questions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            return View(question);
        }

        // POST: Questions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Question question = db.Questions.Find(id);
            db.Questions.Remove(question);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetAuthStudent()
        {
            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            var student = AccountUtils.AuthenticatedUser(cookie);

            return student != null ? student.Name : null;
        }
    }
}
